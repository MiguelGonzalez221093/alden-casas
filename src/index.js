import 'bootstrap/scss/bootstrap-grid.scss';
import 'slick-carousel/slick/slick';
import 'slick-carousel/slick/slick.css';
import './js/index';
import './styles/styles.sass';

import { Menu, PropertiesCarousel, Testimonials, Gallery, ScrollSpy } from './js';

new Menu();
new PropertiesCarousel();
new Testimonials();
new Gallery();

window.addEventListener("load", ()=>{
    document.querySelectorAll(".featured-homes").forEach((section,i)=>{
        section.querySelectorAll(".spy").forEach((spy,i)=>{
            new ScrollSpy({
                element: section,
                item: spy
            }).spy();
        });
    })
    document.querySelectorAll(".about").forEach((section,i)=>{
        section.querySelectorAll(".spy").forEach((spy,i)=>{
            new ScrollSpy({
                element: section,
                item: spy
            }).spy();
        });
    })
    document.querySelectorAll(".testimonials").forEach((section,i)=>{
        section.querySelectorAll(".spy").forEach((spy,i)=>{
            new ScrollSpy({
                element: section,
                item: spy
            }).spy();
        });
    })
    document.querySelectorAll(".buy-sell").forEach((section,i)=>{
        section.querySelectorAll(".spy").forEach((spy,i)=>{
            new ScrollSpy({
                element: section,
                item: spy
            }).spy();
        });
    })
    document.querySelectorAll(".contact").forEach((section,i)=>{
        section.querySelectorAll(".spy").forEach((spy,i)=>{
            new ScrollSpy({
                element: section,
                item: spy
            }).spy();
        });
    })
})