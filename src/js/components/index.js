import Menu from './menu';
import PropertiesCarousel from './properties-carousel';
import Testimonials from './testimonials';
import Gallery from './gallery';
import ScrollSpy from './scroll-spy';

export {
    Menu,
    PropertiesCarousel,
    Testimonials,
    Gallery,
    ScrollSpy
}