class ScrollSpy {
    constructor({
        element,
        item
    }){
        this.element = element
        this.item = item
        if(this._isVisible()){
            this.item.classList.add('animate')
        }
    }

    spy(){
        document.addEventListener("scroll", ()=>{
            if(this._isVisible()){
                this.item.classList.add('animate')
            }
        })
    }
    _isVisible(){
        return !this.item.classList.contains('animate') && (window.scrollY + window.innerHeight)>=(this.element.offsetTop + (this.item.offsetHeight / 2))
    }
}

export default ScrollSpy;