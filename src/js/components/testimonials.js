class Testimonials {
    constructor(){
        this.el = document.querySelector(".testimonials-carousel");
        $(this.el).slick({
            slidesToShow: 2,
            slidesToScroll: 2,
            dots: false,
            prevArrow: $(".testimonials-carousel-button.nav-left"),
            nextArrow: $(".testimonials-carousel-button.nav-right"),
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                }
            ]
        })
    }
}

export default Testimonials;