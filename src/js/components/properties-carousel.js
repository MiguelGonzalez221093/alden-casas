class PropertiesCarousel {
    constructor(){
        this.el = document.querySelector(".property-cards-carousel");
        this.el.querySelectorAll(".property").forEach((item,i)=>{
            item.style.transitionDelay = i*1000+"ms";
        });
        $(this.el).slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            dots: false,
            arrows: false,
            responsive: [
                {
                    breakpoint: 992,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                },
                {
                    breakpoint: 737,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        infinite: true,
                        dots: true
                    }
                }
            ]
        })
    }
}

export default PropertiesCarousel;