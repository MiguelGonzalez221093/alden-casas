class Menu {
    constructor(){
        this.menuButton = document.querySelector("nav .menu-button");
        this.menu = document.querySelector("nav .menu");
        this.init();
    }

    init(){
        window.addEventListener("scroll", ()=>{
            if(window.scrollY >= document.querySelector("#main-banner").offsetHeight)
                document.querySelector("nav").classList.add("fixed");
            else
                document.querySelector("nav").classList.remove("fixed");
        })
        this.menuButton.addEventListener("click", ()=>{
            if(this.menuButton.classList.contains("open")){
                this.menuButton.classList.remove("open");
                this.menu.classList.remove("open");
                this.menu.classList.add("close");
                setTimeout(()=>{
                    document.querySelector("html").style.overflow = "inherit";
                    this.menu.classList.remove("close");
                },1550);
            }else{
                document.querySelector("html").style.overflow = "hidden";
                this.menuButton.classList.add("open");
                this.menu.classList.add("open");
            }
        })
    }
}

export default Menu;